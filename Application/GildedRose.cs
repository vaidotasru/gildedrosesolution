﻿using System;
using System.Collections.Generic;

namespace Application
{
    public class GildedRose
    {
        readonly IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                UpdateItemQuality(item);
            }
        }

        private void UpdateItemQuality(Item item)
        {
            if (item.Name.StartsWith("Sulfuras"))
                return;

            int qualityDiff;

            if (item.Name.Equals("Aged Brie"))
            {
                qualityDiff = item.SellIn <= 0 ? 2 : 1;
                item.Quality = Math.Min(item.Quality + qualityDiff, 50);
            }
            else if (item.Name.StartsWith("Backstage passes"))
            {
                if (item.SellIn > 10)
                {
                    qualityDiff = 1;
                } 
                else if (item.SellIn <= 10 && item.SellIn > 5)
                {
                    qualityDiff = 2;
                }
                else if (item.SellIn <= 5 && item.SellIn > 0)
                {
                    qualityDiff = 3;
                }
                else
                {
                    qualityDiff = -item.Quality;
                }

                item.Quality = Math.Min(item.Quality + qualityDiff, 50);
            }
            else if (item.Name.StartsWith("Conjured"))
            {
                qualityDiff = item.SellIn <= 0 ? 4 : 2;
                item.Quality = Math.Max(item.Quality - qualityDiff, 0);
            }
            else
            {
                qualityDiff = item.SellIn <= 0 ? 2 : 1;
                item.Quality = Math.Max(item.Quality - qualityDiff, 0);
            }

            item.SellIn -= 1;
        }
    }
}
