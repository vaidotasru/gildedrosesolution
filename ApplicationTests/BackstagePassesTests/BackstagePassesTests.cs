﻿using Application;
using NUnit.Framework;
using System.Collections.Generic;

namespace BackstagePassesTests
{
    public class BackstagePassesTests
    {
        [Test]
        public void TestBackstagePassQualityIncrease()
        {
            var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 20, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(11, items[0].Quality, "Invalid Backstage pass quality increase");
        }
        [Test]
        public void TestBackstagePassQualityNotExceedFifty()
        {
            var items = new List<Item> { 
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 20, Quality = 50 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 10, Quality = 49 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 5, Quality = 48 }
            };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.LessOrEqual(items[0].Quality, 50, "Backstage pass quality exceeds 50");
            Assert.LessOrEqual(items[1].Quality, 50, "Backstage pass quality exceeds 50");
            Assert.LessOrEqual(items[2].Quality, 50, "Backstage pass quality exceeds 50");
        }

        [Test]
        public void TestBackstagePassQualityBellowOrEqualTenDaysIncrease()
        {
            var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 10, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(12, items[0].Quality, "Invalid Backstage pass quality bellow or equal 10 days increase");
        }

        [Test]
        public void TestBackstagePassQualityBellowOrEqualFiveDaysIncrease()
        {
            var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 5, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(13, items[0].Quality, "Invalid Backstage pass quality bellow or equal 5 days increase");
        }

        [Test]
        public void TestBackstagePassQualityDropAfterExpiration()
        {
            var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].Quality, "Backstage pass quality does not drop to 0 after expiration");
        }

        [Test]
        public void TestBackstagePassQualityNotNegative()
        {
            var items = new List<Item> { 
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 1 }, 
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 0 } 
            };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].Quality, "Backstage pass quality is negative");
            Assert.AreEqual(0, items[1].Quality, "Backstage pass quality is negative");

        }

        [Test]
        public void TestBackstagePassesSellInDegrade()
        {
            var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 2, Quality = 0 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(1, items[0].SellIn, "Backstage pass Sell in date does not degrade");
        }
    }
}