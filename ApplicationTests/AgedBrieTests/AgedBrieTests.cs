﻿using Application;
using NUnit.Framework;
using System.Collections.Generic;

namespace AgedBrieTests
{
    public class AgedBrieTests
    {
        [Test]
        public void TestAgedBrieQualityIncrease()
        {
            var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 2, Quality = 0 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(1, items[0].Quality, "Aged Brie item quality does not increase");
        }

        [Test]
        public void TestExpiredAgedBrieQualityIncrease()
        {
            var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 0 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(2, items[0].Quality, "Aged Brie item quality does not increase");
        }

        [Test]
        public void TestAgedBrieQualityNotExceedQualityLimit()
        {
            var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 49 }, new Item { Name = "Aged Brie", SellIn = 1, Quality = 50 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(50, items[0].Quality, "Aged Brie item quality exceeds limit");
            Assert.AreEqual(50, items[1].Quality, "Aged Brie item quality exceeds limit");

        }

        [Test]
        public void TestAgedBrieSellInDegrade()
        {
            var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 2, Quality = 0 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(1, items[0].SellIn, "Aged Brie Sell in date does not degrade");
        }
    }
}