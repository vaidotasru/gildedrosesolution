using Application;
using NUnit.Framework;
using System.Collections.Generic;

namespace BaseItemTests
{
    public class BaseItemTests
    {
        [Test]
        public void TestBaseItemQualityDegrade()
        {
            var items = new List<Item> { new Item { Name = "Unspecial item", SellIn = 1, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(9, items[0].Quality, "Invalid base item quality degrade");
        }

        [Test]
        public void TestExpiredBaseItemQualityDegrade()
        {
            var items = new List<Item> { new Item { Name = "Unspecial item", SellIn = 0, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(8, items[0].Quality, "Base item quality does not degrade by 2 after sell in date has expired");
        }

        [Test]
        public void TestBaseItemQualityNotNegative()
        {
            var items = new List<Item> {
                new Item { Name = "Unspecial item", SellIn = 0, Quality = 1 }, 
                new Item { Name = "Unspecial item", SellIn = 1, Quality = 0 }
            };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].Quality, "Base item quality dropped to negative value");
            Assert.AreEqual(0, items[1].Quality, "Base item quality dropped to negative value");
        }

        [Test]
        public void TestBaseItemSellInDegrade()
        {
            var items = new List<Item> { new Item { Name = "Unspecial item", SellIn = 1, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].SellIn, "Invalid base item Sell in degrade");
        }
    }
}