﻿using Application;
using NUnit.Framework;
using System.Collections.Generic;

namespace SulfurasTests
{
    public class SulfurasTests
    {
        [Test]
        public void TestSulfurasQualityDoesNotDegrade()
        {
            var items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 10, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(10, items[0].Quality, "Sulfuras quality degrades");
        }

        [Test]
        public void TestSulfurasSellInDoesNotDegrade()
        {
            var items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 10, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(10, items[0].SellIn, "Sulfuras Sell In degrades");
        }
    }
}