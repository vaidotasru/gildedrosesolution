﻿using Application;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConjuredTests
{
    public class ConjuredTests
    {
        [Test]
        public void TestConjuredItemQualityDegrade()
        {
            var items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(8, items[0].Quality, "Invalid conjured item quality degrade");
        }

        [Test]
        public void TestExpiredConjuredItemQualityDegrade()
        {
            var items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 0, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(6, items[0].Quality, "Invalid expired conjured item degrade");
        }

        [Test]
        public void TestConjuredItemQualityNotNegative()
        {
            var items = new List<Item> {
                new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 0 }, 
                new Item { Name = "Conjured Mana Cake", SellIn = 0, Quality = 1 } 
            };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].Quality, "Conjured item quality dropped to negative value");
            Assert.AreEqual(0, items[1].Quality, "Conjured item quality dropped to negative value");
        }

        [Test]
        public void TestConjuredItemSellInDegrade()
        {
            var items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 1, Quality = 10 } };
            var gildedRose = new GildedRose(items);
            gildedRose.UpdateQuality();

            Assert.AreEqual(0, items[0].SellIn, "Invalid Conjured item Sell in degrade");
        }
    }
}